
public class ItemNotFoundException extends LinkedListException {

	public ItemNotFoundException() {
		super("Object not found");
	}
	
	public ItemNotFoundException(String msg) {
		super(msg);
	}
}
