
public class MainDniSortedArray {

	public static void main(String[] args) throws NifException  {
		SortedArray<DniGen> arr = new SortedArray<DniGen>(5);
		
		arr.putProfe(new DniGen(12345678));
		arr.putProfe(new DniGen(20914366));
		arr.putProfe(new DniGen(18957690));
		
		for(int i=0; i<arr.getNunElements();i++) {
			System.out.println(arr.getElementAt(i).toFormatedString());
		}
	}

}
