
public class EmptyListException extends ItemNotFoundException {
	public EmptyListException() {
		
		super("The list is empty");
	}
	public EmptyListException(String msg) {
		super(msg);
	}
}
