
public class LinkedList<E> {
	private Element<E> first;
	private Element<E> last;

	public LinkedList() {
		first = null;
		last = null;
	}

	public void insertFirst(E item) {
		Element<E> e = new Element<E>(item);

		if (first == null) {
			last = e;
		} else {
			e.setNext(first);
		}
		first = e;
	}

	public void insertLast(E item) {
		Element<E> e = new Element<E>(item);
		if (first == null) {
			first = e;
		} else {
			last.setNext(e);
		}
		last = e;

	}

	public void print() {
		Element<E> e = first;
		while (e != null) {
			System.out.println(e.getItem());
			e = e.getNext();
		}

	}

	public boolean isEmpty() {
		if (first == null) {
			return true;
		} else {
			return false;
		}
	}

	public void remove(E item) throws ItemNotFoundException, EmptyListException {

		if (isEmpty()) {
			throw new EmptyListException();
		}

		Element<E> current, previous;

		current = first;
		previous = first;

		while (current != null && !current.getItem().equals(item)) {
			previous = current;
			current = current.getNext();
		}
		if (current == null) {
			throw new ItemNotFoundException("Element " + item + " not found in the list");
		}
		
		if (first == last) {
			first = null;
			last = null;
		} else {
			if (current == first) {
				first = current.getNext();
			} else {
				previous.setNext(current.getNext());
			}
			if (current == last) {
				last = previous;
			}
		}

	}

	public E getFirstItem() throws EmptyListException {
		if (isEmpty()) {
			EmptyListException e = new EmptyListException("The list is empty");
			throw e;
		}
		return first.getItem();
	}

	public E getLastObject() throws EmptyListException {
		if (isEmpty()) {
			EmptyListException e = new EmptyListException("The list is empty");
			throw e;
		}
		return last.getItem();
	}

	public int getNumElements() {
		Element<E> e = first;
		int cont = 0;
		while (e != null) {
			cont++;
			e = e.getNext();
		}
		return cont;
	}

	public E getItemAtPosition(int i) throws EmptyListException, InvalidIndexException {
		if (isEmpty()) {
			EmptyListException e = new EmptyListException("The list is empty");
			throw e;
		}

		if (i < 0) {
			throw new InvalidIndexException();
		}
		Element<E> e = first;
		int cont = 1;
		while (e != null) {
			if (cont == i) {
				return e.getItem();
			} else {
				cont++;
				e = e.getNext();
			}
		}
		if (cont <= i) {
			InvalidIndexException index = new InvalidIndexException("Invalid index");
			throw index;
		}

		throw new Error("This should never happend, if happends YOUR MON IS GAY");
	}
}
