
public class SortedArray<E extends Comparable<E>> {

	private E[] array;
	private int numElements;

	public SortedArray(int longArr) {
		array =(E[]) new Comparable[longArr];
		numElements = 0;

		for (int i = 0; i < array.length; i++) {
			array[i] = null;
		}
	}
	/*public SortedArray(int longArr, boolean r) {
		array =(E[]) new Object [longArr];
		numElements = longArr;

		for (int i = 0; i < array.length; i++) {
			array[i] = i;
		}
	}*/

	

	public void putProfe(E n) {
		int i = 0;
		while (i < numElements && array[i].compareTo(n) <0) {
			i++;
		}
		// i has now the position in which to insert
		for (int j = numElements; j > i; j--) {
			if (j < array.length) {
				array[j] = array[j - 1];
			}
		}
		if (i < array.length) {
			array[i] = n;
		}
		if (numElements < array.length) {
			numElements++;
		}
	}

	@Override
	public String toString() {
		String s = "";
		boolean first = true;
		int i = 0;
		while (array[i] != null) {
			if (first) {
				s += "" + array[i];
				first = false;
			} else {
				s += "," + array[i];
			}
			i++;
		}
		return s;
	}

	public int getNunElements() {
		return numElements;
	}

	public int getSize() {
		return array.length;
	}

	public E getElementAt(int number) {
		return array[number];
	}

	public void removeElement(int n) {
		if(n>= numElements) {return ;}
		for (int i = n; i < numElements - 1; i++) {
			array[i] = array[i + 1];
		}

		array[numElements - 1] = null;
		numElements--;

	}

	public boolean isEmpty() {
		return (array[0] != null);
	}

	public boolean isFull() {
		return (array[getSize() - 1] != null);
	}
	
	public boolean existElement(E n) {
		int i=0;
		while(i< numElements && array[i].compareTo(n) <0) {
			
			i++;
		}
		if(i< numElements && array[i]==n) {
				return true;
			}else {
				return false;
			}
		
	}
	public boolean existElement2(E n) {
		return binarySearch(n, 0, numElements-1);
	}
	
	public boolean binarySearch(E n,int minorIndex,int upperIndex) {
		int middle = ((minorIndex + upperIndex)/2);
		if(minorIndex>upperIndex) {
			return false;
		}else if(array[middle].compareTo(n) == 0) {
			return true;
		}else {
			if(n.compareTo(array[middle]) <0) {
				return binarySearch(n, minorIndex, middle-1);
			}else {
				return binarySearch(n, middle+1, upperIndex);
			}
		}
	}

}
