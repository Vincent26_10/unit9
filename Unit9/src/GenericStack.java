
import java.util.ArrayList;

public class GenericStack<T> {
	private ArrayList<T> matrix;

	public GenericStack() {
		matrix = new ArrayList<T>();
	}

	public void push(T object) {
		matrix.add(0, object);
	}

	public T pop() {
		T obj;

		obj = matrix.get(0);
		matrix.remove(0);
		return obj;
	}

	public void empty() {
		matrix.clear();
	}

	public boolean isEmpty() {
		return matrix.get(0) == null;
	}

}
