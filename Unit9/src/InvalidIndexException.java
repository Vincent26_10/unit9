
public class InvalidIndexException extends LinkedListException{
	public InvalidIndexException() {
		super("Invalid index exception");
	}
	
	public InvalidIndexException(String msg) {
		super(msg);
	}
}
