
public class Queue<E> {
	private LinkedList<E> lin;

	public Queue() {
		lin = new LinkedList<E>();
	}
	
	public void push(E obj) {
		lin.insertLast(obj);
	}
	
	public E pop() throws EmptyListException, ItemNotFoundException {
		E ob = lin.getFirstItem();
		lin.remove(lin.getFirstItem());
		return ob;

	}
	
	public boolean isEmpty() {
		return lin.isEmpty();
	}
	
	public void empty() throws EmptyListException,ItemNotFoundException {
		int val = 0;
		val = lin.getNumElements();
		for (int i = 0; i < val; i++) {

				lin.remove(lin.getFirstItem());

		}
	}

	public void Print() {
		lin.print();
	}

}
