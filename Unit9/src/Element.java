
public class Element<E> {
	private E item;
	private Element<E> next;
	
	public Element(E newItem) {
		item=newItem;
		next = null;
	}
	
	public void setNext(Element<E> newNext) {
		next=newNext;
	}
	
	public Element<E> getNext() {
		return next;
	}
	
	public E getItem() {
		return item;
	}
	
	public void setObject (E newItem) {
		item=newItem;
	}
	 public void delete() {
		 item=null;
		 next=null;
	 }
	
	
}
