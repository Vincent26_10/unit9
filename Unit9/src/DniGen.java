
public class DniGen implements Comparable<DniGen> {
	private int number;
	private char letter;
	private static char[] LETTERS = { 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S',
			'Q', 'V', 'H', 'L', 'C', 'K', 'E' };

	public DniGen() {
		number = 00000000;
		letter = LETTERS[0];
	}

	public DniGen(int n, char c) {
		number = n;
		letter = c;
		checkLetter();
	}

	public DniGen(int n) {
		number = n;
		letter = LETTERS[n % 23];
	}

	public DniGen(String s) {
		letter = s.charAt(s.length() - 1);
		s = s.replaceAll("[^\\d.]", "");
		number = Integer.valueOf(s);
		checkLetter();

	}

	public int getNumber() {
		return number;
	}

	public char getLetter() {
		return letter;
	}

	public void setNumber(int number) {
		this.number = number;
		checkLetter();

	}

	public void checkLetter() {
		if (number > 0) {
			if (LETTERS[number % 23] != letter) {
				this.number = -number;
			}
		} else {
			if (LETTERS[-number % 23] == letter) {
				number = -number;
			}
		}
	}

	public boolean isCorrect() {
		if (number > 0) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public String toString() {
		return "" + number + letter;
	}

	public String toFormatedString() {
		StringBuilder s = new StringBuilder(String.valueOf(number));
		if (number > 999) {
			s.insert(s.length() - 3, '.');
		}
		if (number > 999999) {
			s.insert(s.length() - 7, '.');
		}
		if (number > 999999999) {
			s.insert(s.length() - 11, '.');
		}

		return s.toString() + "-" + letter;

	}

	public boolean correctDni() {
		if (number < 0) {
			return false;
		} else {
			return true;
		}
	}

	public static char letterForDni(int number) {
		return LETTERS[number % 23];
	}

	public static String NifForDni(int number) {
		return new DniGen(number).toString();
	}

	@Override
	public int compareTo(DniGen dni) {
		if (letter > dni.getLetter()) {
			return 1;
		} else if (letter < dni.getLetter()) {
			return -1;
		} else if (number > dni.getNumber()) {
			return 1;
		} else if (number < dni.getNumber()) {
			return -1;
		} else {
			return 0;
		}
	}
}
