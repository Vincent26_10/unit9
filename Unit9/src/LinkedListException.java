
public class LinkedListException  extends Exception{
	
	public LinkedListException() {
		super("Linked list exception");
	}
	public LinkedListException(String msg) {
		super(msg);
	}
}
